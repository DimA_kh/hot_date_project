WEB application for dating


Authorization and Registration Module

    • The user has the opportunity to register in the system:
        • Required fields: Email, Password, Enter your password again, Name, Gender, Age;
        • Optional fields: City, Phone, Show my phone, Avatar, Interests, I would like to meet, Age from, Age up to (hidden fields Films, Music, Sport, Literature);
        • Required email confirmation;
        • The link in the email checks the token and, if successful, redirects to the main page of the site;
        • The user has the ability to authenticate on the website (enter email and password);
        • The user has the ability to Log out of his account;


User profile

    • The user has the ability to view his profile:
        • The user has the ability to edit his profile data:
            • Upload an avatar;
            • Choose gender;
            • Prefered gender search;
            • Interests;
            • Description about yourself;
            • Preferences in music;
            • Preferences in literature;
            • Movie preferences;
            • Preferences in sports;
            • Age;
            • Current city;
            • Telephone;
            • Show phone number to everyone or not;
            • Age limit for search;
            • Name.
        • Add albums (Cover, Photo, Description);
        • Add no more than 20 profile photos;
        • On the album page, it is possible to add new photos to this album, create / change the description of the album, delete the album (with mandatory confirmation), open photos, delete photos and view the list of users who like this photo;
        • View users who have subscribed with the ability to go to the subscriber's profile;
        • The user has the opportunity to go to the chat menu, where all started dialogs are displayed, with the ability to go further either to the interlocutor's profile or to the chat room;
        • Delete your profile, indicating the reason for deletion, as well as the required confirmation of deletion;
    
    • The user has the ability to view the profiles of other users:
        • The user has the opportunity to write a message;
        • The user is able to rate the profile of another user (only once);
        • Subscribe to another user;
        • It is possible to go to the album page, view photos and put likes;
        • The user has the opportunity to send a complaint against another user by filling out a form describing the complaint (when considering a complaint, a letter is sent to the user who filled a complaint with information that the complaint has been considered and measures have been taken).


Messages

    • The user has the ability to send messages to other users in real time;
    • When a chat is loaded, the message history of the given dialog is loaded.


Search

    The user has the ability to search for other users by various criteria:
        • Name;
        • City of residence;
        • Search preferences (gender, age);
        • By interest;
        • Preferences in music;
        • Movie preferences;
        • Literature preferences;
        • Preferences in sports;
        • By the presence or absence of an avatar;
        • Sort results by rating and age.
    As a search result, 8 users are displayed on the page (paginator implemented).


Administrator

     • Administrator must be able to block users (when blocked, the user receives email explaining what happened and why he was banned, and where to go to resolve this issue, and the user does not have the opportunity to visit any page of the site);
     • Administrator should be able to view complaints and mark them as done.


Main page

    • For an unregistered user, a login form and a registration link are shown, as well as the top 12 most recently registered users;
    • For a registered user on the main page, a list of Top 12 people with the same interests with the highest rating is shown.


Designed with Bootstrap, CSS, JavaScript, jQuery
