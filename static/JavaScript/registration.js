$('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(2)').hide();
$('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(3)').hide();
$('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(4)').hide();
$('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(5)').hide();

$('#id_interest_0').change(function() {
    if (this.checked) {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(2)').show();
    } else {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(2)').hide();
    }
})

$('#id_interest_1').change(function() {
    if (this.checked) {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(3)').show();
    } else {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(3)').hide();
    }
})

$('#id_interest_2').change(function() {
    if (this.checked) {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(4)').show();
    } else {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(4)').hide();
    }
})

$('#id_interest_3').change(function() {
    if (this.checked) {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(5)').show();
    } else {
        $('#mainBody > div:nth-child(5) > div > div > form > div:nth-child(4) > div:nth-child(5)').hide();
    }
})
