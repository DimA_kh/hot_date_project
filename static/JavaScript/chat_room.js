const $container = $('.chat-window');
const room_id = $('#room_id').val();
const current_user_id = $('#current_user_id').val();
const chatWS = new WebSocket(`ws:${window.location.host}/ws/chat/${room_id}/`);

chatWS.onmessage = function (e) {
    const data = JSON.parse(e.data);
    const new_message = JSON.parse(data.message);
    if (new_message['message_user_id'] === Number(current_user_id)) {
        let message = `<div class="my_message">
                           <p>
                               <img class="circle" src="${new_message['image']}" alt=""> &nbsp;
                               ${new_message['first_name']} &nbsp; ${new_message['created_at']}
                           </p>
                           <p>${new_message['text']}</p>
                       </div>`;
        $container.append(message);
        $container[0].scrollTop = 9e9;
    } else {
        let message = `<div class="companion_message">
                           <p>
                               ${new_message['created_at']} &nbsp; ${new_message['first_name']} &nbsp;
                               <img class="circle" src="${new_message['image']}" alt="">
                           </p>
                           <p>${new_message['text']}</p>
                       </div>`;
        $container.append(message);
        $container[0].scrollTop = 9e9;
    }
};

$('#send_message').click(function (){
    chatWS.send(JSON.stringify({
        'type': "message",
        'message': $('#text_message').val(),
    }));
    $('#text_message').val('');
});

