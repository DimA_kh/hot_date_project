$(document).ready(function() {
    $('.add_like').click(function () {
        const csrftoken = $('[name="csrfmiddlewaretoken"]').val();
        const photo_id = $(this).data('id');
        let data = {
            csrfmiddlewaretoken: csrftoken,
            photo_id_like: photo_id,
        };
        $.post(window.location.href, data, function (data) {
            if (data.is_like) {
                $('#photoID_' + photo_id).html(data['count']);
            } else {
                $('#photoID_' + photo_id).html(data['count']);
            }
        });
    });
});


