$(document).ready(function() {
    $('#subscribe, #unsubscribe').click(function () {
        let data = {
            csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val()
        };
        let btn = $(this);
        $.post(window.location.href, data, function (data) {
            if (data.is_subscribe) {
                btn.removeClass('btn-outline-success').addClass('btn-outline-danger');
                btn.text('Unsubscribe')
            } else {
                btn.removeClass('btn-outline-danger').addClass('btn-outline-success');
                btn.text('Subscribe')
            }
        });
    });
});
