from django.core.mail import send_mail
from hot_date.celery import app


@app.task
def send_user_block_notification(user_id):
    from website.models import SiteUser

    site_user = SiteUser.objects.get(id=user_id)
    send_mail(
        f'YOU ARE BLOCKED ON THE SITE HOT_DATE!',
        f'Hello, {site_user.first_name}!\n'
        f'You have been blocked on the site Hot_date due to violation of the rules for using the site!\n'
        f'For all questions, contact the site administration at hotdate.ukraine@gmail.com',
        'hotdate.ukraine@gmail.com',
        recipient_list=[site_user.email]
    )

@app.task
def send_complaint_handling_notice(from_user_id, to_user_name, date_complaint):
    from website.models import SiteUser

    from_user = SiteUser.objects.get(id=from_user_id)
    send_mail(
        f'Your complaint has been processed!',
        f'Hello, {from_user.first_name}!\n'
        f'Your complaint against user {to_user_name} ({date_complaint}) has been considered.',
        'hotdate.ukraine@gmail.com',
        recipient_list=[from_user.email]
    )
