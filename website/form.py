import re

from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from .models import *
from .widgets import *


class ProfileUpdateForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'phone',
            'show_phone',
            'gender',
            'age',
            'city',
            'interest',
            'literature',
            'music',
            'sport',
            'film',
            'about_me',
            'avatar',
            'search_gender',
            'search_min_age',
            'search_max_age',
        )
        widgets = {
            'first_name': forms.widgets.TextInput(attrs={'placeholder': 'max 12 characters',
                                                         'class': 'form-control'}),
            'age': forms.widgets.TextInput(),#NumberInput(),
            'phone': forms.widgets.TextInput(attrs={'placeholder': '+38 093 1234567',
                                                    'class': 'form-control'}),
            'show_phone': forms.widgets.RadioSelect(choices=((True, True), (False, False))),
            'interest': forms.widgets.CheckboxSelectMultiple(),
            'sport': forms.widgets.CheckboxSelectMultiple(),
            'music': forms.widgets.CheckboxSelectMultiple(),
            'literature': forms.widgets.CheckboxSelectMultiple(),
            'film': forms.widgets.CheckboxSelectMultiple(),
            'gender': forms.widgets.Select(choices=((None, _('-----')),
                                                    ('male', _('Male')),
                                                    ('female', _('Female')))),
            'search_gender': forms.widgets.RadioSelect(choices=((None, _('Nobody')),
                                                                ('all', _('All')),
                                                                ('male', _('Male')),
                                                                ('female', _('Female')))),
            'about_me': forms.Textarea(attrs={'placeholder': 'I...', 'class': 'form-control'}),
            'avatar': SelfImageWidget()
        }

    def clean_search_min_age(self):
        min_age = self.cleaned_data['search_min_age']
        if not min_age:
            min_age = 18
        if 18 > min_age:
            self.add_error('search_min_age', 'The minimum search age must be 18 years')
        elif min_age > 100:
            self.add_error('search_min_age', 'Error choose search age')
        return min_age

    def clean_search_max_age(self):
        if 'search_min_age' in self.cleaned_data:
            max_age = self.cleaned_data['search_max_age']
            if not max_age:
                max_age = 100
            if self.cleaned_data['search_min_age'] > max_age:
                self.add_error('search_max_age', 'The maximum search age must be greater than the minimum search age')
            elif max_age > 100:
                self.add_error('search_max_age', 'Error choose search age')
            return max_age

    def clean_age(self):
        age = self.cleaned_data['age']
        if not age or 18 > age or age > 100:
            self.add_error('age', 'Input error in age')
        return age

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        if not phone is None and not re.match(r'^(\+)?((\d{2,3}) ?\d|\d)(([ -]?\d)|( ?(\d{2,3}) ?)){5,12}\d$', phone):
            self.add_error('phone', 'Invalid number')
        return phone


class RegistrationForm(ProfileUpdateForm):
    password = forms.CharField(
        widget=forms.PasswordInput,
        min_length=6,
        help_text='Minimum 6 characters',
    )
    check_password = forms.CharField(
        widget=forms.PasswordInput,
        min_length=6,
        help_text='Repeat your password',
    )

    class Meta:
        model = get_user_model()
        fields = (
            'email',
            'first_name',
            'phone',
            'show_phone',
            'gender',
            'age',
            'city',
            'interest',
            'film',
            'music',
            'sport',
            'literature',
            'about_me',
            'avatar',
            'search_gender',
            'search_min_age',
            'search_max_age',
        )
        widgets = {
            'first_name': forms.widgets.TextInput(attrs={'placeholder': 'max 12 characters',
                                                         'class': 'form-control'}),
            'age': forms.widgets.NumberInput(attrs={'class': 'form-control'}),
            'phone': forms.widgets.TextInput(attrs={'placeholder': '+38 022 1234567',
                                                    'class': 'form-control'}),
            'show_phone': forms.widgets.RadioSelect(choices=((True, True), (False, False))),
            'interest': forms.widgets.CheckboxSelectMultiple(),
            'sport': forms.widgets.CheckboxSelectMultiple(),
            'music': forms.widgets.CheckboxSelectMultiple(),
            'literature': forms.widgets.CheckboxSelectMultiple(),
            'film': forms.widgets.CheckboxSelectMultiple(),
            'gender': forms.widgets.Select(choices=((None, ('-----')),
                                                    ('male', _('Male')),
                                                    ('female', _('Female')))),
            'search_gender': forms.widgets.RadioSelect(choices=((None, _('Nobody')),
                                                                ('all', _('All')),
                                                                ('male', _('Male')),
                                                                ('female', _('Female')))),
            'about_me': forms.Textarea(attrs={'placeholder': 'I...', 'class': 'form-control'}),
            'avatar': SelfImageWidget()
        }

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Passwords do not match')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        self.save_m2m()
        return user


class AddAlbumForm(forms.ModelForm):
    cover_album = forms.ImageField(widget=forms.FileInput(attrs={'multiple': False}))

    class Meta:
        model = Album
        fields = ('title', 'description')

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        super(AddAlbumForm, self).__init__(*args, **kwargs)

    def clean_title(self):
        exist_album = Album.objects.filter(user=self.instance.user.id, title=self.cleaned_data['title']).exists()
        if exist_album:
            raise forms.ValidationError('Album with that name already exists.')
        return self.cleaned_data['title']

    def clean_cover_album(self):
        cover_album = self.request.FILES.get('cover_album')
        if 'image' in cover_album:
            raise forms.ValidationError('Error uploaded cover.')
        return self.cleaned_data['cover_album']


class AddPhotoForm(forms.Form):
    photos = forms.ImageField(widget=forms.FileInput(attrs={'multiple': True}))

    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs.pop('request')
        super(AddPhotoForm, self).__init__(*args, **kwargs)

    def clean_photos(self):
        photos = [photo for photo in self.request.FILES.getlist('photos') if 'image' in photo.content_type]
        if len(photos) == 0:
            raise forms.ValidationError('Not found uploaded photos.')
        return photos

    def save_for(self, album):
        for photo_url in self.cleaned_data['photos']:
            Photo(photo_url=photo_url, album=album).save()


class ComplaintForm(forms.ModelForm):

    class Meta:
        model = Complaint
        fields = (
            'title',
            'complaint_text',
        )
        widgets = {
            'title': forms.widgets.Select(choices = (('other', _('Other')),
                                                     ('vulgar photos', _('Vulgar photos')),
                                                     ('abusive language', _('Abusive language')),
                                                     ('obscene proposals', _('Obscene proposals')),
            )),
            'complaint_text': forms.widgets.Textarea(attrs={'placeholder': '...', 'class': 'form-control'})
        }


class ProfileDeleteForm(forms.ModelForm):

    class Meta:
        model = ProfileDelete
        fields = (
            'reason_for_deletion',
            'description',
        )
        widgets = {
            'reason_for_deletion': forms.widgets.Select(choices =
                                             (("other", _("Other")),
                                              ("a lot of inappropriate content", _("A lot of inappropriate content")),
                                              ("I'm tired", _("I'm tired")),
                                              ("found a couple", _("Found a couple")),
                                              ("don't like this site", _("Don't like this site")),
                                              )
            ),
            'description': forms.widgets.Textarea(attrs={'placeholder': '...', 'class': 'form-control'})
        }



