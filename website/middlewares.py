import re

from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.utils.deprecation import MiddlewareMixin
from django.utils.http import url_has_allowed_host_and_scheme
from hot_date import settings


exempt_URL = [re.compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    exempt_URL += [re.compile(url) for url in settings.LOGIN_EXEMPT_URLS]


class UserCheckMiddleware(MiddlewareMixin):
    """Verification of authorized and blocked user"""
    def process_request(self, request):
        assert hasattr(request, 'user'), "The Login Required Middleware"
        if not request.user.is_authenticated and 'activate' in request.META['QUERY_STRING'].split('/'):
            return HttpResponseRedirect(request.META['QUERY_STRING'][5:])
        elif not request.user.is_authenticated:
            path = request.path_info.lstrip('/')
            if not any(m.match(path) for m in exempt_URL):
                redirect_to = settings.LOGIN_URL
                if len(path) > 0 and url_has_allowed_host_and_scheme(url=request.path_info,
                                                                     allowed_hosts=request.get_host()):
                    redirect_to = f"{settings.LOGIN_URL}?next={request.path_info}"
                return HttpResponseRedirect(redirect_to)
        if request.user.is_authenticated and request.user.block_account:
            return HttpResponseForbidden(
                '''<br>
                   <h1 align="center">Your page has been blocked for violating site rules</h1>
                   <hr>
                   <h3 align="center">For all questions, contact the site administration at <u><i>hotdate.ukraine@gmail.com</i></u></h3>'''
            )


