from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import EmailField
from django.utils.translation import gettext, gettext_lazy as _
from .models import ALL_MODELS, SiteUser
from django import forms


class CreateUser(UserCreationForm):
    password1 = forms.CharField(label=_("Password"))
    password2 = forms.CharField(label=_("Password confirmation"))

    class Meta:
        model = User
        fields = ("email",)
        field_classes = {"email": EmailField}


class UserProfileAdmin(UserAdmin):
    add_form = CreateUser
    list_filter = ('block_account', 'is_active', 'is_staff', 'is_superuser')
    list_display = ('email', 'is_active', 'first_name', 'gender', 'is_staff', 'block_account')
    search_fields = ('id', 'email', 'first_name')
    ordering = ()
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2"),
            },
        ),
    )
    # readonly_fields = (
    #     'first_name',
    #     'email',
    #     'phone',
    #     'show_phone',
    #     'age',
    #     'gender',
    #     'city',
    #     'avatar',
    #     'avg_rating',
    #     'about_me',
    #     'interest',
    #     'sport',
    #     'film',
    #     'music',
    #     'literature',
    #     'search_gender',
    #     'search_min_age',
    #     'search_max_age'
    # )
    fieldsets = (
        (None, {
            'fields': ('email', 'password', 'avg_rating')
        }),

        (_('BLOCK ACCOUNT'), {
            'fields': ('block_account',)
        }),

        (_('Personal info'), {
            'fields': ('first_name', 'gender', 'age', 'phone', 'city', 'show_phone')
        }),

        (_('Info account'), {
            'fields': ('avatar', 'about_me')
        }),

        (_('Subscribers'), {
            'fields': ('subscribers',)
        }),

        (_('Info interests'), {
            'fields': ('interest', 'sport', 'film', 'music', 'literature')
        }),

        (_('Info search'), {
            'fields': ('search_gender', 'search_min_age', 'search_max_age')
        }),

        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),

        (_('Important dates'), {
            'fields': ("date_joined",)
        }),
    )


for model in ALL_MODELS:
    admin.site.register(model)

admin.site.register(SiteUser, UserProfileAdmin)
