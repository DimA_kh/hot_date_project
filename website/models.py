from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, UserManager
from django.db import models
from django.db.models import Avg
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from website.tasks import (
    send_complaint_handling_notice,
    send_user_block_notification
)


class MyUserManager(UserManager):
    """User creation manager"""
    def _create_user(self, email, password, **extra_fields):
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        if not extra_fields.get('is_staff'):
            raise ValueError('Superuser mast have is staff equals True.')
        if not extra_fields.get('is_superuser'):
            raise ValueError('Superuser mast have is superuser equals True.')
        return self._create_user(email, password, **extra_fields)


class TimeIt(models.Model):
    """Abstract model for creating and updating an entity"""
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class TitleIt(models.Model):
    """Abstract model for creating the name of a new entity"""
    title = models.CharField(max_length=100, blank=True, null=True,)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


def user_avatar_upload_to(instance, file_name):
    """Automatic generation of user avatar folder"""
    return f'avatar/user_id_{instance.pk}/{file_name}'

def album_cover_upload_to(instance, file_name):
    """Automatic generation of album cover folder"""
    return f'cover/album_id_{instance.pk}/{file_name}'

def photo_upload_to(instance, file_name):
    """Automatic generation of photo folder"""
    user_id = Album.objects.filter(id=instance.album_id).values_list('user', flat=True).first()
    return f'foto/user_{user_id}/album_{instance.album}/{file_name}'


class SiteUser(AbstractBaseUser, PermissionsMixin):
    """User base model"""
    email = models.EmailField(_('email'), max_length=100, unique=True)
    first_name = models.CharField(_('first_name'), max_length=12)
    phone = models.CharField(_('phone'), max_length=15, unique=True, blank=True, null=True)
    show_phone = models.BooleanField(_('show_phone'), default=False)
    age = models.IntegerField(_('age'), blank=True, null=True)
    gender = models.CharField(_('gender'), max_length=10)
    avatar = models.ImageField(_('avatar'), upload_to=user_avatar_upload_to, blank=True, null=True)
    is_avatar = models.BooleanField(default=False)
    is_active = models.BooleanField(_('active'), default=True)
    is_staff = models.BooleanField(_('staff status'), default=False, help_text=_("Is the admin site available."))
    username = models.CharField(max_length=3, blank=True, null=True)
    block_account = models.BooleanField(_('block account'), default=False)
    city = models.ForeignKey('website.City', on_delete=models.SET_NULL, blank=True, null=True)
    interest = models.ManyToManyField('website.InterestUser', blank=True, null=True, related_name='self_interest')
    sport = models.ManyToManyField('website.Sport', blank=True, null=True, related_name='sports')
    film = models.ManyToManyField('website.Film', blank=True, null=True, related_name='genre_film')
    music = models.ManyToManyField('website.Music', blank=True, null=True, related_name='genre_music')
    literature = models.ManyToManyField('website.Literature', blank=True, null=True, related_name='genre_literature')
    about_me = models.TextField(_('about_me'), blank=True, null=True)
    avg_rating = models.FloatField(_('rating'), default=0)
    search_gender = models.CharField(_('search gender'), max_length=15, blank=True, null=True)
    search_min_age = models.IntegerField(_('search min age'), default=18, blank=True, null=True)
    search_max_age = models.IntegerField(_('search max age'), default=100, blank=True, null=True)
    subscribers = models.ManyToManyField('website.SiteUser', blank=True, null=True)
    date_joined = models.DateTimeField(default=timezone.now)

    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = verbose_name
        indexes = [
            models.Index(fields=['city',]),
            models.Index(fields=['first_name',]),
        ]

@receiver(pre_save, sender=SiteUser)
def avatar_field_changes(instance, **kwargs):
    if instance.avatar:
        instance.is_avatar = True
    else:
        instance.is_avatar = False

@receiver(post_save, sender=SiteUser)
def user_added_to_blacklist(instance, **kwargs):
    """block_account field control, and when block_account == True, send changes to tasks"""
    if instance.block_account:
        send_user_block_notification.delay(instance.id)


class ProfileDelete(models.Model):
    """Data of deleted users and reasons for deletion"""
    user_email = models.CharField(max_length=100)
    reason_for_deletion = models.CharField(max_length=30)
    description = models.CharField(max_length=300, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class RatingVote(models.Model):
    current_user = models.IntegerField()
    vote = models.IntegerField()
    score = models.PositiveSmallIntegerField(default=0)


@receiver(post_save, sender=RatingVote)
def rating_handler(instance, **kwargs):
    """Average user rating calculation and change in the main rating model"""
    SiteUser.objects.filter(id=instance.current_user)\
                    .update(
                        avg_rating=RatingVote.objects
                        .filter(current_user=instance.current_user)
                        .aggregate(Avg('score'))['score__avg']
                    )


class Album(TitleIt, TimeIt):
    slug = models.SlugField(unique=True, db_index=True, verbose_name='URL')
    description = models.CharField(max_length=150, blank=True, null=True)
    cover_album = models.ImageField(upload_to=album_cover_upload_to)
    user = models.ForeignKey('website.SiteUser', on_delete=models.CASCADE, blank=True, null=True, related_name='album')

    def save(self, *args, **kwargs):
        self.slug = slugify(f'{str(self.user_id)}_{self.title}')
        super(Album, self).save(*args, **kwargs)


class Photo(TimeIt):
    album = models.ForeignKey('website.Album', on_delete=models.CASCADE, blank=True, null=True, related_name='photo')
    photo_url = models.ImageField(upload_to=photo_upload_to)
    like = models.ManyToManyField('website.SiteUser', blank=True, null=True,)


class City(TitleIt):
    pass


class ChatRoom(models.Model):
    name = models.CharField(max_length=20, blank=True, null=True)
    host_user = models.ForeignKey('website.SiteUser', on_delete=models.CASCADE, related_name='chat')
    companion_user = models.ForeignKey('website.SiteUser', on_delete=models.CASCADE,
                                     related_name='current_chat', blank=True)


class Message(TimeIt):
    user = models.ForeignKey('website.SiteUser', on_delete=models.CASCADE, related_name='messages')
    room = models.ForeignKey('website.ChatRoom', on_delete=models.CASCADE, related_name='room_messages')
    content = models.CharField(max_length=600)


class Complaint(TitleIt, TimeIt):
    title = models.CharField('reason for deletion', max_length=100, blank=True, null=True)
    complaint_text = models.CharField(max_length=500)
    processed = models.BooleanField(default=False)
    from_user = models.IntegerField()
    to_user = models.ForeignKey('website.SiteUser', on_delete=models.CASCADE, related_name='complaint_user')

@receiver(post_save, sender=Complaint)
def complaint_processed(instance, **kwargs):
    """Control over changes in the processed field"""
    if instance.processed:
        date_complaint = instance.created_at.strftime('%d/%M/%Y %H:%M')
        send_complaint_handling_notice.delay(instance.id, instance.to_user.first_name, date_complaint)


class InterestUser(TitleIt):
    pass


class Film(TitleIt):
    pass


class Music(TitleIt):
    pass


class Literature(TitleIt):
    pass


class Sport(TitleIt):
    pass


ALL_MODELS = [
    ChatRoom,
    Message,
    Complaint,
    Photo,
    Album,
    City,
    InterestUser,
    Sport,
    Literature,
    Music,
    Film,
    RatingVote
]
