from django.contrib.auth import login
from django.contrib.auth.views import LoginView
from django.db.models import Count, Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import (
    DeleteView,
    DetailView,
    FormView,
    ListView,
    RedirectView,
    TemplateView
)
from .filter import UserFilter
from .form import *
from .models import *
from .utils.email_util import send_registration_email
from .utils.token_generator import TokenGenerator


class MainView(TemplateView):
    """The main page where users are sorted by rating and users are filtered by interests"""
    template_name = 'index.html'
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        self.current_user = request.user
        self.interests_current_user = tuple(self.current_user.interest.all().values_list(flat=True))
        return super(MainView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        site_users = get_user_model().objects.filter(is_superuser=False, is_active=True) \
                            .exclude(id=self.current_user.id) \
                            .filter(interest__in=self.interests_current_user) \
                            .order_by('-avg_rating').distinct()[:12]
        context = super(MainView, self).get_context_data(**kwargs)
        context.update({
            'site_users': site_users.values('id', 'avatar', 'age', 'first_name', 'gender'),
        })
        return context


class AboutView(TemplateView):
    template_name = 'about.html'


class SelfLoginView(LoginView):
    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super(SelfLoginView, self).get_context_data(**kwargs)
        site_users = SiteUser.objects.filter(is_superuser=False, is_active=True).order_by('-date_joined')[:12]
        context.update({
            'site_users': site_users,
        })
        return context


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/'

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.is_active = False
        self.object.save()
        send_registration_email(request=self.request, user_instance=self.object)
        return super(RegistrationView, self).form_valid(form)


class ActivateUserView(RedirectView):
    """Decryption and verification of the link that was sent from the mail when registering on the site"""
    url = '/'

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            id = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=id)
        except (get_user_model().DoesNotExist, TypeError, ValueError):
            return HttpResponse('<h1 style="margin-top:100px; margin-left:400px">Wrong data! User does not exist!</h1>')
        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()
            login(request, current_user)
            return super(ActivateUserView, self).get(request, *args, **kwargs)
        return HttpResponse('<h1 style="margin-top:100px; margin-left:400px;">Wrong data! Link is not valid!</h1>')


class ProfileView(DetailView):
    """Guest profile:
        - view the user's open data,
        - possibility of subscription,
        - rate the profile,
        - go to the user's albums,
        - go to the page for writing a complaint against this user,
        - go to the chat with the user
    Private profile:
        - view user data,
        - go to the subscriber view page,
        - go to the page for changing profile data,
        - go to user albums,
        - go to the page of all chats of the user """
    model = get_user_model()
    template_name = 'profile.html'
    pk_url_kwarg = 'id'

    def dispatch(self, request, *args, **kwargs):
        self.current_user = request.user
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        site_user = get_user_model().objects.filter(id=self.kwargs['id'])
        if self.kwargs['id'] != self.current_user.id:
            companion_user = get_user_model().objects.get(id=self.kwargs['id'])
            chat = ChatRoom.objects.filter(Q(host_user=self.current_user, companion_user=companion_user) |
                                           Q(host_user=companion_user, companion_user=self.current_user)).first()
            not_exists_rating = False
            if not RatingVote.objects.filter(current_user=self.kwargs['id'], vote=self.current_user.id).exists():
                not_exists_rating = True
            if not chat:
                chat = ChatRoom.objects.create(host_user=self.current_user, companion_user=companion_user)
            context.update({
                'not_exists_rating': not_exists_rating,
                'chat_id': chat.id,
            })
        interests = InterestUser.objects.filter(id__in=site_user.values_list('interest', flat=True)) \
                                        .values_list('title', flat=True)
        films = Film.objects.filter(id__in=site_user.values_list('film', flat=True)) \
                            .values_list('title', flat=True)
        music = Music.objects.filter(id__in=site_user.values_list('music', flat=True)) \
                             .values_list('title', flat=True)
        sports = Sport.objects.filter(id__in=site_user.values_list('sport', flat=True)) \
                              .values_list('title', flat=True)
        literature = Literature.objects.filter(id__in=site_user.values_list('literature', flat=True)) \
                                       .values_list('title', flat=True)
        context.update({
            'site_user': site_user,
            'interests': interests,
            'films': films,
            'sports': sports,
            'music': music,
            'literature': literature,
            'user_id': self.current_user.id,
        })
        return context

    def post(self, request, id):
        site_user = get_object_or_404(get_user_model(), id=id)
        is_subscribe = True
        if request.user not in site_user.subscribers.all():
            site_user.subscribers.add(request.user)
        else:
            site_user.subscribers.remove(request.user)
            is_subscribe = False
        site_user.save()
        return JsonResponse({'is_subscribe': is_subscribe})


class NewRatingView(TemplateView):
    """Processing new user rating"""

    def post(self, request, *args, **kwargs):
        current_user_id = request.POST['current_user_id']
        RATING_DICT = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5}
        if request.POST.get('radiobutton') in RATING_DICT \
                and not RatingVote.objects.filter(current_user=current_user_id, vote=request.user.id).exists():
            RatingVote.objects.create(
                current_user=current_user_id,
                score=RATING_DICT[request.POST.get('radiobutton')],
                vote=request.user.id
            )
        return redirect(f'/profile/{current_user_id}')


class ProfileUpdateView(TemplateView):
    template_name = 'update_profile.html'

    def dispatch(self, request, *args, **kwargs):
        self.current_user = request.user
        return super(ProfileUpdateView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        user_id = self.kwargs['id']
        context = super(ProfileUpdateView, self).get_context_data()
        context.update({
            'form': ProfileUpdateForm(instance=self.current_user),
            'user_id': user_id,
        })
        return context

    def post(self, request, id):
        form = ProfileUpdateForm(data=request.POST, files=request.FILES, instance=self.current_user)
        if form.is_valid():
            form.save()
            return redirect(f'/profile/{id}')
        return self.render_to_response(context={'form': form})


class ProfileDeleteView(DeleteView):
    model = SiteUser
    form_class = ProfileDeleteForm
    pk_url_kwarg = 'id'
    template_name = 'delete_user.html'

    def get_context_data(self, **kwargs):
        self.current_user = kwargs['object']
        context = super(ProfileDeleteView, self).get_context_data()
        form_delete = self.form_class()
        context.update({
            'form_delete': form_delete,
            'user_id': self.current_user.id,
        })
        return context

    def post(self, request, *args, **kwargs):
        form_delete = self.form_class(request.POST)
        form_delete.instance.user_email = request.user.email
        if form_delete.is_valid():
            form_delete.save()
            request.user.delete()
            return redirect('/')
        return self.form_invalid(form_delete)


class SubscribersView(ListView):
    model = SiteUser
    paginate_by = 12
    template_name = 'subscribers.html'
    pk_url_kwarg = 'id'

    def get_queryset(self, **kwargs):
        return get_user_model().objects.get(id=self.kwargs['id']).subscribers.filter(is_superuser=False)\
                                                                             .values('id', 'avatar', 'first_name')

    def get_context_data(self, **kwargs):
        user_id = self.kwargs['id']
        context = super(SubscribersView, self).get_context_data(**kwargs)
        context.update({
            'user_id': user_id,
        })
        return context


class AddAlbumView(TemplateView):
    model = Photo
    template_name = 'add_album.html'
    form_album_class = AddAlbumForm
    form_photos_class = AddPhotoForm

    def dispatch(self, request, *args, **kwargs):
        self.current_user_id = request.user.id
        return super(AddAlbumView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AddAlbumView, self).get_context_data()
        context.update({
            'form_album': self.form_album_class(),
            'form_photo': self.form_photos_class(),
            'user_id': self.current_user_id,
        })
        return context

    def post(self, request, id):
        form_album = self.form_album_class(request.POST, request.FILES, request=request)
        form_album.instance.user = self.request.user
        form_photo = self.form_photos_class(request.POST, request.FILES, request=request)
        photo_counter = len(request.FILES.getlist('photos'))
        if form_album.is_valid() and form_photo.is_valid() and photo_counter <= 20:
            album = form_album.save()
            form_photo.save_for(album)
            album.cover_album = form_album.clean_cover_album()
            album.save()
            return redirect(f'/profile/{id}')
        return self.render_to_response(context={'form_album': form_album, 'form_photo': form_photo})


class AlbumView(DetailView):
    model = Album
    template_name = 'album.html'
    slug_url_kwarg = 'slug'

    def dispatch(self, request, *args, **kwargs):
        self.current_user_id = request.user.id
        return super(AlbumView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        user_id = kwargs['object'].__dict__['user_id']
        context = super(AlbumView, self).get_context_data(**kwargs)
        photos = Photo.objects.filter(album__user_id=user_id, album__title=kwargs['object'])\
                              .annotate(likes=Count('like'))
        context.update({
            'photos': photos,
            'user_id': user_id,
            'current_user_id': self.current_user_id,
        })
        return context

    def post(self, request, slug):
        if request.POST.get('photo_id_like'):
            photo = Photo.objects.get(id=request.POST.get("photo_id_like"))
            is_like = True
            if request.user not in photo.like.all():
                photo.like.add(request.user)
            else:
                photo.like.remove(request.user)
                is_like = False
            photo.save()
            return JsonResponse({'is_like': is_like, 'count': photo.like.count()})
        if request.POST.get('delete_album'):
            get_object_or_404(Album, pk=request.POST['delete_album']).delete()
            return redirect(f'/profile/{self.current_user_id}')
        elif request.POST.get('photo_delete'):
            get_object_or_404(Photo, pk=request.POST['photo_delete']).delete()
        elif request.POST.get('album_description'):
            album = Album.objects.get(slug=slug)
            album.description = request.POST['album_description']
            album.save()
        elif request.FILES.getlist('add_photo'):
            photos = [photo for photo in request.FILES.getlist('add_photo') if 'image' in photo.content_type]
            photo_counter = Photo.objects.filter(album__user=request.user).count() + len(photos)
            if len(photos) > 0 and photo_counter <= 20:
                for photo_url in photos:
                    Photo(photo_url=photo_url, album=Album.objects.get(slug=slug)).save()
        return redirect(request.path)


class LikesView(ListView):
    model = Photo
    paginate_by = 12
    pk_url_kwarg = 'id'
    template_name = 'likes.html'

    def get_queryset(self):
        users = Photo.objects.filter(id=self.kwargs['id']).values_list('like', flat=True)
        return get_user_model().objects.filter(id__in=users, is_superuser=False).values('id', 'avatar', 'first_name')

    def get_context_data(self, **kwargs):
        context = super(LikesView, self).get_context_data(**kwargs)
        album_slug = Photo.objects.get(id=self.kwargs['id']).album.slug
        context.update({
            'album_slug': album_slug,
        })
        return context


class ComplaintView(TemplateView):
    model = Complaint
    template_name = 'complaint.html'
    form_complain_class = ComplaintForm

    def get_context_data(self, **kwargs):
        context = super(ComplaintView, self).get_context_data()
        form_complain = self.form_complain_class()
        context.update({
            'form_complain': form_complain,
            'user_id': kwargs['id'],
        })
        return context

    def post(self, request, id):
        form_complain = self.form_complain_class(request.POST)
        form_complain.instance.from_user = request.user.id
        form_complain.instance.to_user = get_user_model().objects.get(id=id)
        if form_complain.is_valid():
            form_complain.save()
            return redirect(f'/profile/{id}')
        return self.render_to_response(context={'form_complain': form_complain})


class SearchView(ListView):
    model = SiteUser
    paginate_by = 8
    template_name = 'search_users.html'
    filter_class = UserFilter

    def get_queryset(self):
        user_list = self.filter_class(self.request.GET, queryset=SiteUser.objects.all()
                          .filter(is_superuser=False, is_active=True)
                          .exclude(id=self.request.user.id)
                          .exclude(block_account=True)
                          .values('id', 'first_name', 'gender', 'age', 'avatar'))
        return user_list.qs

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data()
        user_list = self.filter_class(self.request.GET, queryset=SiteUser.objects.all())
        context.update({
            'user_list': user_list,
        })
        return context


class ChatsView(ListView):
    model = ChatRoom
    paginate_by = 10
    template_name = 'all_chats.html'

    def dispatch(self, request, *args, **kwargs):
        self.current_user = request.user
        return super(ChatsView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        return ChatRoom.objects.filter(Q(host_user=self.current_user) | Q(companion_user=self.current_user)) \
                               .annotate(messages_count=Count('room_messages')) \
                               .filter(messages_count__gte=1)

    def get_context_data(self, **kwargs):
        context = super(ChatsView, self).get_context_data(**kwargs)
        context.update({
            'user_id': self.current_user.id,
        })
        return context


class ChatRoomView(TemplateView):
    """Displaying the chat page, as well as the history of correspondence (the last 100 messages)"""
    template_name = 'chat_room.html'

    def dispatch(self, request, *args, **kwargs):
        self.current_user = request.user
        return super(ChatRoomView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ChatRoomView, self).get_context_data(**kwargs)
        room = get_object_or_404(ChatRoom, id=self.kwargs['id'])
        companion = get_user_model().objects.get(id=room.companion_user.id)
        host_user = get_user_model().objects.get(id=room.host_user.id)
        messages = Message.objects.filter(room=room)[:100]
        context.update({
            'messages': list(messages.values('user', 'created_at', 'content')),
            'room': room,
            'current_user': self.current_user,
            'host_user': host_user,
            'companion': companion,
        })
        return context
