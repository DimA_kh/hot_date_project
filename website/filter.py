import django_filters
from django import forms

from website.models import City, Film, InterestUser, Literature, Music, Sport


class UserFilter(django_filters.FilterSet):
    """User filter by specified parameters"""
    first_name = django_filters.CharFilter(field_name='first_name', lookup_expr='contains')
    gender = django_filters.ChoiceFilter(field_name='gender',
                                         choices=(('male', 'Male'), ('female', 'Female')))
    age__gte = django_filters.NumberFilter(field_name='age', lookup_expr='gte')
    age__lte = django_filters.NumberFilter(field_name='age', lookup_expr='lte')
    city = django_filters.ModelChoiceFilter(queryset=City.objects.all())
    photo = django_filters.ChoiceFilter(field_name='is_avatar',
                                        choices=((True, 'Only with photo'), (False, 'Without photo')))
    preferred_gender = django_filters.ChoiceFilter(field_name='search_gender',
                                        choices=(('male', 'Male'), ('female', 'Female'), ('all', 'Male&Female')))
    interest = django_filters.ModelMultipleChoiceFilter(queryset=InterestUser.objects.all(),
                                                        widget=forms.CheckboxSelectMultiple)
    film = django_filters.ModelMultipleChoiceFilter(queryset=Film.objects.all(),
                                                        widget=forms.CheckboxSelectMultiple)
    music = django_filters.ModelMultipleChoiceFilter(queryset=Music.objects.all(),
                                                        widget=forms.CheckboxSelectMultiple)
    sport = django_filters.ModelMultipleChoiceFilter(queryset=Sport.objects.all(),
                                                     widget=forms.CheckboxSelectMultiple)
    literature = django_filters.ModelMultipleChoiceFilter(queryset=Literature.objects.all(),
                                                      widget=forms.CheckboxSelectMultiple)
    ordering = django_filters.OrderingFilter(fields=(('age', 'Age'), ('avg_rating', 'Rating')))








