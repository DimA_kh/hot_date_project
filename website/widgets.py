from django.forms import ClearableFileInput


class SelfImageWidget(ClearableFileInput):
    """User avatar widget in ProfileUpdateView"""
    template_name = 'widgets/image.html'

    def get_context(self, name, value, attrs):
        context = super(SelfImageWidget, self).get_context(name, value, attrs)
        context['value'] = value
        return context
