from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from website.models import InterestUser, RatingVote


class MainPageViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        InterestUser.objects.create(id=1, title='test')
        interest = InterestUser.objects.all()
        number_of_users = 5
        for site_user_num in range(number_of_users):
            user = get_user_model().objects.create(
                id=site_user_num,
                first_name=f'test_{str(site_user_num)}',
                email=f'test_user_{str(site_user_num)}@test.com',
                age=site_user_num + 20,
                gender='male',
                avg_rating=site_user_num+2/3,
            )
            user.set_password('secret')
            user.interest.set(interest)
            user.save()

    def setUp(self):
        self.client = Client()
        self.client.login(email='test_user_1@test.com', password='secret')

    def test_diagnostic(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code != 404)

    def test_diagnostic_index_page(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code == 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_get_context_data(self):
        expected_value = 4
        result_count = get_user_model().objects.filter(is_superuser=False, is_active=True) \
                                               .exclude(id=1) \
                                               .filter(interest=1) \
                                               .only('age', 'first_name') \
                                               .distinct() \
                                               .count()
        result_user = get_user_model().objects.filter(is_superuser=False, is_active=True) \
                                              .exclude(id=1) \
                                              .filter(interest=1) \
                                              .only('age', 'first_name') \
                                              .distinct() \
                                              .order_by('-avg_rating')[0]
        self.assertEqual(expected_value, result_count)
        self.assertEqual(expected_value, result_user.id)


class LoginViewTest(TestCase):

    def setUp(self):
        self.client = Client()

    def test_correctly_redirect_login_url_(self):
        response = self.client.get('/')
        self.assertTrue(response.status_code == 302)

    def test_login_url_correctly(self):
        response = self.client.get('/login/')
        self.assertTrue(response.status_code == 200)
        self.assertTemplateUsed(response, 'login.html')


class RegistrationViewTest(TestCase):

    def test_registration_url_correctly(self):
        response = self.client.get('/registration/')
        self.assertTrue(response.status_code == 200)
        self.assertTemplateUsed(response, 'registration.html')


class ProfileViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_users = 3
        for site_user_num in range(number_of_users):
            user = get_user_model().objects.create(
                id=site_user_num,
                first_name=f'test_{str(site_user_num)}',
                email=f'test_user_{str(site_user_num)}@test.com',
                age=site_user_num + 20,
                gender='male',
                avg_rating=site_user_num + 2 / 3,
            )
            user.set_password('secret')
            user.save()

    def setUp(self):
        self.client = Client()
        self.client.login(email='test_user_1@test.com', password='secret')
        self.user = get_user_model().objects.get(id=1)

    def test_profile_url_correctly(self):
        user = get_user_model().objects.get(id=0)
        response = self.client.get(f'/profile/{user.id}')
        self.assertTrue(response.status_code == 200)
        self.assertTemplateUsed(response, 'profile.html')

    def test_get_context_data_self_profile(self):
        response = self.client.get(f'/profile/{self.user.id}').context_data
        self.assertFalse('chat_id' in response and 'not_exists_rating' in response)

    def test_get_context_data_guest_profile(self):
        user = get_user_model().objects.get(id=0)
        expected_not_exists_rating = False
        expected_chat_id = 1
        RatingVote.objects.create(current_user=user.id, vote=self.user.id)
        response = self.client.get(f'/profile/{user.id}').context_data
        self.assertTrue('chat_id' in response and 'not_exists_rating' in response)
        self.assertEqual(expected_not_exists_rating, response['not_exists_rating'])
        self.assertEqual(expected_chat_id, response['chat_id'])

    def test_post_add_rating(self):
        expected_count_rating_vote = 1
        user = get_user_model().objects.get(id=0)
        self.client.post(f'/profile/{user.id}', {'radiobutton': '3'})
        result_count_rating_vote = RatingVote.objects.all().count()
        rating_vote = RatingVote.objects.first()
        self.assertEqual(expected_count_rating_vote, result_count_rating_vote)
        self.assertEqual(self.user.id, rating_vote.vote)
        self.assertEqual(user.id, rating_vote.current_user)

    def test_subscribe(self):
        expected_is_subscribe = True
        user = get_user_model().objects.get(id=0)
        response = self.client.post(f'/profile/{user.id}')
        self.assertEqual(user.subscribers.all().count(), 1)
        self.assertEqual(user.subscribers.first().id, self.user.id)
        self.assertEqual(expected_is_subscribe, response.json()['is_subscribe'])

    def test_unsubscribe(self):
        expected_is_subscribe = False
        user = get_user_model().objects.get(id=0)
        user.subscribers.add(self.user)
        response = self.client.post(f'/profile/{user.id}')
        self.assertEqual(user.subscribers.all().count(), 0)
        self.assertEqual(expected_is_subscribe, response.json()['is_subscribe'])


class ActivateUserTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        user = get_user_model().objects.create(
            id=0,
            first_name='test_user_',
            email='test_user_0@test.com',
            age=20,
            gender='male',
            is_active=False,
        )
        user.set_password('secret')
        user.save()

    def test_get_incorrect_user(self):
        non_existent_user_id = 1
        with self.assertRaises(Exception):
            get_user_model().objects.get(pk=non_existent_user_id)

