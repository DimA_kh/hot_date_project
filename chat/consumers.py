import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.contrib.auth import get_user_model
from website.models import ChatRoom, Message
from sorl.thumbnail import get_thumbnail


class ChatConsumer(WebsocketConsumer):
    """Processing messages from the chat, writing messages to the database"""
    def connect(self):
        print('CONNECT')
        self.room_id = self.scope['url_route']['kwargs']['id']
        self.group_name = f'room_{self.room_id}'
        self.room = ChatRoom.objects.get(id=self.room_id)
        self.companion_user = self.room.companion_user
        if not self.room.name:
            self.room.name = self.group_name
            self.room.save()
        self.user = self.scope['user']
        self.accept()

        async_to_sync(self.channel_layer.group_add)(
            self.group_name,
            self.channel_name,
        )

    def disconnect(self, close_code):
        print('DISCONNECT')
        async_to_sync(self.channel_layer.group_discard)(
            self.group_name,
            self.channel_name,
        )

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        first_name = get_user_model().objects.get(id=self.user.id).first_name
        if text_data_json['type'] == 'message':
            message = text_data_json['message']
            Message.objects.create(user=self.user, room=self.room, content=message)
            created_at = Message.objects.last().created_at.strftime("%d.%m.%Y, %H:%M")
            image = ''
            if self.user.avatar:
                image = get_thumbnail(self.user.avatar, '60x60', crop='center', quality=99).url
            async_to_sync(self.channel_layer.group_send)(
                self.group_name,
                {
                    'type': "chat_message",
                    'message': json.dumps({
                        'message_user_id': self.user.id,
                        'first_name': first_name,
                        'created_at': created_at,
                        'image': image,
                        'text': message
                    }),
                }
            )

    def chat_message(self, event):
        message = event["message"]
        self.send(text_data=json.dumps({
            'message': message,
            'type': "message"
        }))
