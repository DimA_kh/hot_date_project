from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from website.views import *


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', MainView.as_view()),
    path('about/', AboutView.as_view()),
    path('login/', SelfLoginView.as_view(template_name='login.html',), name='login'),
    path('registration/', RegistrationView.as_view()),
    path('activate/<str:uuid64>/<str:token>/', ActivateUserView.as_view(), name='activate'),
    path('logout/', LogoutView.as_view()),
    path('profile/<int:id>', ProfileView.as_view()),
    path('profile/rating/', NewRatingView.as_view()),
    path('profile/<int:id>/subscribers/', SubscribersView.as_view()),
    path('profile/<int:id>/update/', ProfileUpdateView.as_view()),
    path('profile/<int:id>/delete/', ProfileDeleteView.as_view()),
    path('profile/<int:id>/add_album/', AddAlbumView.as_view()),
    path('profile/album/<slug:slug>', AlbumView.as_view()),
    path('profile/album/photo_likes/<int:id>', LikesView.as_view()),
    path('profile/<int:id>/chats/', ChatsView.as_view()),
    path('chat/<int:id>/', ChatRoomView.as_view()),
    path('profile/<int:id>/complaint/', ComplaintView.as_view()),
    path('search/', SearchView.as_view()),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG == True:
    urlpatterns.append(
        path('__debug__/', include('debug_toolbar.urls')),
    )